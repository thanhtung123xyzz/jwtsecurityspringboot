package com.tung.service;

import java.util.ArrayList;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/*
JWTUserDetailsService triển khai giao diện Spring Security UserDetailsService.
Nó ghi đè loadUserByUsername để tìm nạp chi tiết người dùng từ cơ sở dữ liệu bằng tên người dùng.
Trình quản lý xác thực bảo mật mùa xuân gọi phương thức này để lấy chi tiết người dùng từ cơ sở dữ liệu
khi xác thực thông tin người dùng do người dùng cung cấp. Ở đây, chúng tôi đang nhận thông tin chi tiết về người dùng từ một mã cứng
Danh sách người dùng. Trong hướng dẫn tiếp theo, chúng tôi sẽ thêm việc triển khai DAO để tìm nạp Chi tiết người dùng từ Cơ sở dữ liệu.
Ngoài ra, mật khẩu cho người dùng được lưu trữ ở định dạng mã hóa bằng BCrypt.
Trước đây chúng ta đã thấy Spring Boot Security - Password Encoding bằng Bcrypt.
Tại đây bằng cách sử dụng Trình tạo Bcrypt Trực tuyến, bạn có thể tạo Bcrypt cho một mật khẩu.
 */
@Service
public class JwtUserDetailsService implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		if("nguyen".equals(username)) {
			return new User("nguyen", "$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6", new ArrayList<>());
		}else {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
	}
	
	
}
